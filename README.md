# Build

erlc -DLAGER -I PATH-TO-EJABBERD/include mod_domain_filter.erl

Example:

    erlc -DLAGER -I /usr/lib64/ejabberd-16.09/include/ mod_domain_filter.erl 
    
# Install

Copy mod_domain_filter.beam into PATH-TO-EJABBERD/ebin/

Example:

    cp ~/programming/erlang/mod_domain_filter/mod_domain_filter.beam /usr/lib64/ejabberd-16.09/ebin/



