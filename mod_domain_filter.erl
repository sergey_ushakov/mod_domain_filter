-module(mod_domain_filter).

-behaviour(gen_mod).

-include("logger.hrl").
-include("ejabberd.hrl").
-include("jlib.hrl").
-include("mod_roster.hrl").

-export([start/2, stop/1, filter_packet/1]).

-define(LOG_MESSAGE(Message, Args), ?DEBUG(Message++" From ~s@~s, To: ~s@~s", Args)).

start(_Host, _Opts) ->
    ejabberd_hooks:add(filter_packet, global, ?MODULE, filter_packet, 50).

stop(_Host) ->
    ejabberd_hooks:delete(filter_packet, global, ?MODULE, filter_packet, 50).


filter_packet(drop) -> 
    drop;

%% accept any group chat packets
filter_packet({From, To, #xmlel{attrs=[{_, <<"groupchat">>}|_]}}=Input) ->
    ?LOG_MESSAGE("Accepted because groupchat message!", [From#jid.luser, From#jid.lserver, To#jid.luser, To#jid.lserver]),
    Input;

filter_packet({From, To, #xmlel{name = <<"message">>}}=Input) ->
    AcceptedPacket = is_packet_accepted(From, To),
    if AcceptedPacket ->
            Input;
       true ->
            drop
    end;

filter_packet(Input) ->
    %% ?LOG_MESSAGE("Was not filtered because type is not 'message'", [From#jid.luser, From#jid.lserver, To#jid.luser, To#jid.lserver]),
    Input.

is_packet_accepted(From, To) ->
    EjabberdHosts = ejabberd_config:get_option({hosts, global}, fun(V) -> V end),
    ModMucHost = gen_mod:get_module_opt(global,mod_muc,host,fun(V) -> V end,<<"conference.localhost">>),
    IsLocalhost = lists:any(fun(X) -> (X == From#jid.lserver) or (From#jid.lserver == ModMucHost) end, EjabberdHosts),
    WhiteList = gen_mod:get_module_opt(global,?MODULE,whitelist,fun(V) -> V end,[]),
    IsUserInWhitelist = lists:any(fun(X) -> X == erlang:iolist_to_binary([To#jid.luser,<<"@">>,To#jid.lserver]) end, WhiteList),
    Roster = mod_roster:get_roster(To#jid.luser, To#jid.lserver),
    
    if IsLocalhost ->
            % accept any messages from local hosts
            ?LOG_MESSAGE("Accepted because Message from local host!", [From#jid.luser, From#jid.lserver, To#jid.luser, To#jid.lserver]),
            true;
       IsUserInWhitelist ->
            ?LOG_MESSAGE("Accepted because target user in whitelist!", [From#jid.luser, From#jid.lserver, To#jid.luser, To#jid.lserver]),
            true;
       length(Roster) == 0 ->
            ?LOG_MESSAGE("Rejected because User is not in roster (empty roster)!", [From#jid.luser, From#jid.lserver, To#jid.luser, To#jid.lserver]),
            false;
       true ->
            Result = lists:any(fun(X) -> 
                                       {User,Server,_} = X#roster.jid,
                                       (User == From#jid.luser) and (Server == From#jid.lserver) and ((X#roster.subscription == both) or (X#roster.subscription == from)) end,
                               Roster),
            if Result ->
                    ?LOG_MESSAGE("Accepted because User in roster.", [From#jid.luser, From#jid.lserver, To#jid.luser, To#jid.lserver]); 
               true ->
                    ?LOG_MESSAGE("Rejected because User is not in roster!", [From#jid.luser, From#jid.lserver, To#jid.luser, To#jid.lserver])
            end,
            Result
    end.
